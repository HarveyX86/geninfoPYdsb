# Just some simple Python script to generate fake information for a joke or whatever, nothing special - Harvey
import asyncio
import discord
import random
import time
from discord.ext import commands
import json

bot = commands.Bot(command_prefix='.', self_bot=True)

@bot.event
async def on_ready():
    print("Ready to go. - Harvey")
    

@bot.command()
async def dox(ctx, user:discord.User):

        embedD1=discord.Embed(title=f'Indexing information for: {user}', description="Searching...", color=0x020202)
        embedD1.set_thumbnail(url="https://cdn.discordapp.com/attachments/605893599078645771/849999942868664410/loading-buffering.gif")
        embedD1.set_footer(text="written in python by Harvey")

        embedD2=discord.Embed(title=f'Indexing information for: {user}', description="Compiling...", color=0x020202)
        embedD2.set_thumbnail(url="https://cdn.discordapp.com/attachments/605893599078645771/849999942868664410/loading-buffering.gif")
        embedD2.set_footer(text="written in python by Harvey")
        
        nameArray = ['Name1', 'Name2', 'Name3']
        emailArray = ['Email1@gmail.com', 'Email2@gmx.com', 'Email3@outlook.com']
        locationArray = ['Toronto, Ontario, Canada', 'East Syracuse, NY 13057, USA', 'Edgware, London, UK', 'Mansfield, OH, USA', 'Nordhorn, Germany']

        embedDF=discord.Embed(title=f'Record for {user} Successfully Compiled:', description="", color=0x020202)
        embedDF.add_field(name="Name", value=random.choice(nameArray), inline=False)
        embedDF.add_field(name="Email Address", value=random.choice(emailArray), inline=False)
        embedDF.add_field(name="Location", value=random.choice(locationArray), inline=False)
        embedDF.add_field(name="IPv4", value="unresolved.", inline=False)
        embedDF.set_footer(text="written in python by Harvey")

        msg1 = await ctx.send(embed=embedD1)
        await asyncio.sleep(5)      
        await msg1.edit(embed=embedD2)
        await asyncio.sleep(4) 
        await msg1.edit(embed=embedDF)

bot.run("your token", bot=False)

